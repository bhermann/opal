/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org

import scala.language.experimental.macros
import scala.reflect.macros.blackbox.Context
import org.opalj.log.GlobalLogContext
import org.opalj.log.OPALLogger

/**
 * OPAL is a Scala-based framework for the static analysis of Java bytecode
 * that is designed with performance, scalability and adaptability in mind.
 *
 * Its main components are:
 *  - a library (`Common`) that defines functionality that is generally useful
 *    in the context of static analyses.
 *  - a framework for parsing Java bytecode (`Bytecode Infrastructure`) that can be used to
 *    create arbitrary representations.
 *  - a library to create a one-to-one in-memory representation of Java bytecode
 *    (`Bytecode Disassembler`).
 *  - a library to create a representation of Java bytecode that facilitates writing
 *    simple static analyses (`Bytecode Representation` - [[org.opalj.br]]).
 *  - a scalable, easily customizable framework for the abstract interpretation of
 *    Java bytecode (`Abstract Interpretation Framework` - [[org.opalj.ai]]).
 *  - a library to extract dependencies between code elements and to facilitate checking
 *    architecture definitions.
 *
 * ==General Design Decisions==
 *
 * ===Thread Safety===
 * Unless explicitly noted, '''OPAL is thread safe'''. I.e., the classes defined by
 * OPAL can be considered to be thread safe unless otherwise stated.
 * (For example, it is possible to read and process class files concurrently without
 * explicit synchronization on the client side.)
 *
 * ===No `null` Values===
 * Unless explicitly noted, '''OPAL does not `null` values'''
 * I.e., fields that are accessible will never contain `null` values and methods will
 * never return `null`.  '''If a method accepts `null` as a value for a parameter or
 * returns a `null` value it is always explicitly documented.'''
 * In general, the behavior of methods that are passed `null` values is undefined unless
 * explicitly documented.
 *
 * ===No Typecasts for Collections===
 * For efficiency reasons, OPAL sometimes uses mutable data-structures internally.
 * After construction time, these data-structures are generally represented using
 * their generic interfaces (e.g., `scala.collection.{Set,Map}`). However, a downcast
 * (e.g., to add/remove elements) is always forbidden as it would effectively prevent the
 * thread-safety properties. Furthermore, the concrete data-structure is always
 * considered an implementation detail and may change at any time.
 *
 * ===Assertions===
 * OPAL makes heavy use of Scala's '''Assertion Facility''' to facilitate writing correct
 * code. Hence, for production builds (after thorough testing(!)) it is
 * highly recommend to build OPAL again using `-Xdisable-assertions`.
 *
 * @author Michael Eichberg
 */
package object opalj {

    {
        // Log the information whether a production build or a development build is
        // used.
        implicit val logContext = GlobalLogContext
        import OPALLogger.info
        try {
            scala.Predef.assert(false)
            info("OPAL", "Common - Production Build")
        } catch {
            case ae: AssertionError ⇒
                info("OPAL", "Common - Development Build (Assertions are enabled)")
        }
    }

    /**
     * The URL of the webpage of the opal project.
     */
    final val WEBPAGE = "http://www.opal-project.de"

    /**
     * The type of a concrete analysis mode.
     */
    type AnalysisMode = AnalysisModes.Value

    /**
     * The type of the predefined relational operators.
     *
     * See [[org.opalj.RelationalOperators]] for the list of all defined operators.
     */
    type RelationalOperator = RelationalOperators.Value

    /**
     * The type of the predefined binary arithmetic operators.
     *
     * See [[org.opalj.BinaryArithmeticOperators]] for the list of all defined operators.
     */
    type BinaryArithmeticOperator = BinaryArithmeticOperators.Value

    /**
     * The type of the predefined unary arithmetic operators.
     *
     * See [[org.opalj.UnaryArithmeticOperators]] for the list of all defined operators.
     */
    type UnaryArithmeticOperator = UnaryArithmeticOperators.Value

    /**
     * A simple type alias that can be used to communicate that the respective
     * value will/should only take values in the range of unsigned short values.
     */
    final type UShort = Int

    /**
     * A simple type alias that can be used to communicate that the respective
     * value will/should only take values in the range of unsigned byte values.
     */
    final type UByte = Int

    /**
     * Iterates over a given array `a` and calls the given function `f` for
     * each non-null value in the array.
     *
     * @note '''This is a macro.'''
     */
    final def foreachNonNullValue[T <: AnyRef](
        a: Array[T]
    )(
        f: (Int, T) ⇒ Unit
    ): Unit = macro ControlAbstractionsImplementation.foreachNonNullValue[T]

    /**
     * Executes the given function `f` for the first `n` values of the given list.
     * The behavior is undefined if the given list does not have at least `n` elements.
     *
     * @note '''This is a macro.'''
     */
    final def forFirstN[T <: AnyRef](
        l: List[T], n: Int
    )(
        f: (T) ⇒ Unit
    ): Unit = macro ControlAbstractionsImplementation.forFirstN[T]

    /**
     * Converts a given bit mask using an `Int` value into a bit mask using a `Long` value.
     *
     * @note This is not the same as a type conversion as the "sign-bit" is not treated
     *      as such. I.e., after conversion of the `Int` value -1, the `Long` value
     *      will be `4294967295` (both have the same bit mask:
     *      `11111111111111111111111111111111`); in other words, the long's sign bit will
     *      still be `0`.
     */
    final def i2lBitMask(value: Int): Long = {
        (value >>> 16).toLong << 16 | (value & 0xFFFF).toLong
    }

    /**
     * Evaluates the given expression `f` with type `T` the given number of
     * `times` and stores the result in an `IndexedSeq[T]`.
     *
     * ==Example Usage==
     * {{{
     * val result = repeat(15) {
     *      System.in.read()
     * }
     * }}}
     *
     * @note '''This is a macro.'''
     *
     * @param times The number of times the expression `f` is evaluated. The `times`
     *      expression is evaluated exactly once.
     * @param f An expression that is evaluated the given number of times unless an
     *      exception is thrown. Hence, even though `f` is not a by-name parameter,
     *      it behaves in the same way.
     * @return The result of the evaluation of the expression `f` the given number of
     *      times stored in an `IndexedSeq`. If `times` is zero an empty sequence is
     *      returned.
     */
    def repeat[T](
        times: Int
    )(
        f: T
    ): IndexedSeq[T] = macro ControlAbstractionsImplementation.repeat[T]
    // OLD IMPLEMENTATION USING HIGHER-ORDER FUNCTIONS
    // (DO NOT DELETE - TO DOCUMENT THE DESIGN DECISION FOR MACROS)
    //        def repeat[T](times: Int)(f: ⇒ T): IndexedSeq[T] = {
    //            val array = new scala.collection.mutable.ArrayBuffer[T](times)
    //            var i = 0
    //            while (i < times) {
    //                array += f
    //                i += 1
    //            }
    //            array
    //        }
    // The macro-based implementation has proven to be approx. 1,3 to 1,4 times faster
    // when the number of times that we repeat an operation is small (e.g., 1 to 15 times)
    // (which is very often the case when we read in Java class files)

    /**
     * Iterates over the given range of integer values `[from,to]` and calls the given
     * function f for each value.
     *
     * If `from` is smaller or equal to `to`, `f` will not be called.
     *
     * @note '''This is a macro.'''
     */
    def iterateTo(
        from: Int, to: Int
    )(
        f: Int ⇒ Unit
    ): Unit = macro ControlAbstractionsImplementation.iterateTo

    /**
     * Iterates over the given range of integer values `[from,until)` and calls the given
     * function f for each value.
     *
     * If `from` is smaller than `until`, `f` will not be called.
     */
    def iterateUntil(
        from: Int, until: Int
    )(
        f: Int ⇒ Unit
    ): Unit = macro ControlAbstractionsImplementation.iterateUntil
}

/**
 * Implementation of the macros.
 *
 * @author Michael Eichberg
 */
private object ControlAbstractionsImplementation {

    def foreachNonNullValue[T <: AnyRef: c.WeakTypeTag](
        c: Context
    )(
        a: c.Expr[Array[T]]
    )(
        f: c.Expr[(Int, T) ⇒ Unit]
    ): c.Expr[Unit] = {
        import c.universe._

        reify {
            val array = a.splice // evaluate only once!
            val arrayLength = array.length
            var i = 0
            while (i < arrayLength) {
                val arrayEntry = array(i)
                if (arrayEntry ne null) f.splice(i, arrayEntry)
                i += 1
            }
        }
    }

    def forFirstN[T <: AnyRef: c.WeakTypeTag](
        c: Context
    )(
        l: c.Expr[List[T]], n: c.Expr[Int]
    )(
        f: c.Expr[T ⇒ Unit]
    ): c.Expr[Unit] = {
        import c.universe._

        reify {
            var remainingList = l.splice
            val max = n.splice
            var i = 0
            while (i < max) {
                val head = remainingList.head
                remainingList = remainingList.tail
                f.splice(head)
                i += 1
            }
        }
    }

    def repeat[T: c.WeakTypeTag](
        c: Context
    )(
        times: c.Expr[Int]
    )(
        f: c.Expr[T]
    ): c.Expr[IndexedSeq[T]] = {
        import c.universe._

        reify {
            val size = times.splice // => times is evaluated only once
            if (size == 0) {
                IndexedSeq.empty
            } else {
                val array = new scala.collection.mutable.ArrayBuffer[T](size)
                var i = 0
                while (i < size) {
                    val value = f.splice // => we evaluate f the given number of times
                    array += value
                    i += 1
                }
                array
            }
        }
    }

    def iterateTo(
        c: Context
    )(
        from: c.Expr[Int],
        to:   c.Expr[Int]
    )(
        f: c.Expr[(Int) ⇒ Unit]
    ): c.Expr[Unit] = {
        import c.universe._

        reify {
            var i = from.splice
            val max = to.splice // => to is evaluated only once
            while (i <= max) {
                f.splice(i) // => we evaluate f the given number of times
                i += 1
            }
        }
    }

    def iterateUntil(
        c: Context
    )(
        from:  c.Expr[Int],
        until: c.Expr[Int]
    )(
        f: c.Expr[(Int) ⇒ Unit]
    ): c.Expr[Unit] = {
        import c.universe._

        reify {
            var i = from.splice
            val max = until.splice // => until is evaluated only once
            while (i < max) {
                f.splice(i) // => we evaluate f the given number of times
                i += 1
            }
        }
    }
}
